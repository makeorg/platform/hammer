FROM makeorg/docker-sbt

WORKDIR /opt/docker
COPY target/docker /opt/docker
RUN mkdir -p /var/run/secrets

ENV API_HOST="https://api.preprod.makeorg.tech" USERS="1" RAMP_DURATION="500"

RUN sbt Test/compile update


ENTRYPOINT sbt "gatling:testOnly org.make.hammer.api.ApiSimulation" swiftSendReports
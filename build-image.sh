#! /bin/bash

mkdir -p target/docker/project
cp -r src/ target/docker/
cp project/* target/docker/project
cp build.sbt target/docker/

docker build -t 'nexus.prod.makeorg.tech/hammer:latest' .
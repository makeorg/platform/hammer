## Running the Infra :

To start the docker container you can run:

```
make infra-up
```

## Running the Load Test :


To start it, got to your project directory and type:

```
make run
```

or form the sbt console you can run
```
gatling:test
```


package org.make.hammer.api

object Bodies {
  case class TrackingRequest(eventName: String, eventType: String, eventParameters: Option[Map[String, String]])
}

package org.make.hammer.api

import java.util.UUID
import com.typesafe.config.{Config, ConfigFactory}
import io.gatling.core.scenario.Simulation
import io.gatling.http.protocol.HttpProtocolBuilder
import org.make.hammer.SimulationConfig
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

import scala.concurrent.duration.DurationInt
import org.make.hammer.PauseConfiguration

import scala.util.Random

class ApiSimulation extends Simulation with SimulationConfig {

  private val config: Config = ConfigFactory.load()

  private val questionSlug = config.getString("service.question")

  private val httpConf: HttpProtocolBuilder = http
    .baseUrls(baseURL.split(","): _*)
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader(defaultAcceptLanguage)
    .userAgentHeader(defaultUserAgent)
    .header("x-make-source", "core")
    .header("Host", hostname)
    .disableCaching

  implicit val pauseConfiguration: PauseConfiguration = PauseConfiguration.regularUserPausesConfiguration

  config.getString("service.scenario") match {
    case "widget"          => setupWidget
    case "accessible"      => setupAccessible
    case "voteOnly"        => setupVoteOnly
    case "acceptProposals" => setupAcceptProposals
    case _                 => setupAccessible
  }

  def setupWidget: SetUp =
    setUp(
      ApiSimulation
        .widgetDisplayOnlyUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.97, 1).toInt)
            .during(rampDuration.seconds)
        ),
      ApiSimulation
        .widgetNormalUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.02, 1).toInt)
            .during(rampDuration.seconds)
        ),
      ApiSimulation
        .widgetProposingUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.01, 1).toInt)
            .during(rampDuration.seconds)
        )
    ).protocols(httpConf)

  def setupAccessible: SetUp =
    setUp(
      ApiSimulation
        .proposingUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.15, 1).toInt)
            .during(rampDuration.seconds)
        ),
      ApiSimulation
        .normalUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.6, 1).toInt)
            .during(rampDuration.seconds)
        ),
      ApiSimulation
        .signUpUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.15, 1).toInt)
            .during(rampDuration.seconds)
        ),
      ApiSimulation
        .givingUpUser(questionSlug)
        .inject(
          rampUsers(Math.max(users * 0.1, 1).toInt)
            .during(rampDuration.seconds)
        )
    ).protocols(httpConf)

  def setupVoteOnly: SetUp =
    setUp(ApiSimulation.normalUser(questionSlug).inject(rampUsers(users).during(rampDuration.seconds)))
      .protocols(httpConf)

  def setupAcceptProposals: SetUp = {
    setUp(
      ApiSimulation
        .acceptProposals(questionSlug, adminAuthParams.username, adminAuthParams.password)
        .inject(atOnceUsers(1))
    ).protocols(httpConf)
  }

}

object ApiSimulation {

  def firstProposal(questionSlug: String): ScenarioBuilder =
    scenario("first-sequence-proposal")
      .exec(
        Requests
          .questionDetails(questionSlug)
          .check(header("x-session-id").saveAs("sessionId"))
      )
      .exec(addCookie(Cookie("make-session-id", "${sessionId}")))
      .exec(Requests.firstSequenceProposal)

  def voteAndQualify(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("handle-proposal").exec { session =>
      val votes = Seq("agree", "disagree", "neutral")
      val voteValue = Random.shuffle(votes).head

      // Choose vote value
      session.set("voteKey", voteValue)
    }.exec(Requests.vote)
      .pause(pauseConfiguration.mediumPauses.minimumPause, pauseConfiguration.mediumPauses.maximumPause)
      .exec { session =>
        // Choose qualifs
        val numberOfQualifs = Random.nextInt(4)
        val qualifs = Random.shuffle(session("qualifications").as[Seq[String]]).take(numberOfQualifs)
        session.set("selectedQualifications", qualifs)
      }
      .foreach("${selectedQualifications}", "qualificationKey") {
        exec(Requests.qualify)
          .pause(pauseConfiguration.mediumPauses.minimumPause, pauseConfiguration.mediumPauses.maximumPause)
      }

  def voteAndQualifyProposals(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("handle-proposals").foreach("${proposals}", "proposal") {
      exec { session =>
        val (proposalId, proposalKey) = session("proposal").as[(String, String)]
        session
          .set("proposalId", proposalId)
          .set("proposalKey", proposalKey)
      }.exec(voteAndQualify)
        .pause(pauseConfiguration.mediumPauses.minimumPause, pauseConfiguration.mediumPauses.maximumPause)
        .exec(Requests.tracking("click-sequence-next-card"))
    }

  def register(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("register").exec { session => session.set("token", UUID.randomUUID().toString) }
      .exec(Requests.tracking("display-signup-form"))
      .pause(pauseConfiguration.longPauses.minimumPause, pauseConfiguration.longPauses.maximumPause)
      .exec(
        Requests
          .tracking("click-proposal-submit")
          .resources(Requests.tracking("display-signup-form"))
      )
      .pause(pauseConfiguration.longPauses.minimumPause, pauseConfiguration.longPauses.maximumPause)
      .exec(Requests.register)
      .exec(Requests.login)
      .exec(addCookie(Cookie("make-secure", "${accessToken}")))
      .exec(Requests.getMe)
      .exec(Requests.tracking("signup-email-success"))

  def registerAndSubmitProposal(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("submit-proposal")
      .exec(
        Requests
          .tracking("click-proposal-submit-form-open")
      )
      .exec(register)
      .exec(Requests.propose)
      .exec(Requests.tracking("display-proposal-submit-validation"))

  def normalUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("normal-user")
      .exec(
        Requests
          .questionDetails(questionSlug)
          .check(header("x-session-id").saveAs("sessionId"))
      )
      .exec(addCookie(Cookie("make-session-id", "${sessionId}")))
      .exec(Requests.startSequenceByQuestion())
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set("proposals", proposals.zip(proposalKeys))
      }
      .exec(voteAndQualifyProposals)

  def widgetNormalUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("normal-user")
      .exec(getFirstProposalAndVoteAndQualify(questionSlug))
      .exec(Requests.startSequenceByQuestion(enableTracking = false))
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val first = session("firstProposal").as[(String, String)]
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set("proposals", Seq(first) ++ proposals.zip(proposalKeys))
      }
      .exec(voteAndQualifyProposals)

  def givingUpUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("giving-up-user")
      .exec(
        Requests
          .questionDetails(questionSlug)
          .check(header("x-session-id").saveAs("sessionId"))
      )
      .exec(addCookie(Cookie("make-session-id", "${sessionId}")))
      .exec(Requests.startSequenceByQuestion())
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set("proposals", proposals.zip(proposalKeys).take(proposals.size / 2))
      }
      .exec(voteAndQualifyProposals)

  def widgetGivingUpUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("giving-up-user")
      .exec(
        Requests
          .questionDetails(questionSlug)
          .check(header("x-session-id").saveAs("sessionId"))
      )
      .exec(addCookie(Cookie("make-session-id", "${sessionId}")))
      .exec(Requests.startSequenceByQuestion())
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set("proposals", proposals.zip(proposalKeys).take(proposals.size / 2))
      }
      .exec(voteAndQualifyProposals)

  def proposingUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("proposing-user")
      .exec(
        Requests
          .questionDetails(questionSlug)
          .check(header("x-session-id").saveAs("sessionId"))
      )
      .exec(addCookie(Cookie("make-session-id", "${sessionId}")))
      .exec(Requests.startSequenceByQuestion())
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set("proposals", proposals.zip(proposalKeys).take(proposals.size / 2))
      }
      .exec(voteAndQualifyProposals)
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec(registerAndSubmitProposal)
      .exec(Requests.tracking("click-back-button-after-proposal-submit"))
      .exec(Requests.startSequenceByQuestion())
      .exec { session =>
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set(
          "proposals",
          proposals
            .zip(proposalKeys)
            .slice(proposals.size / 2, proposals.size / 2)
        )
      }
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec(voteAndQualifyProposals)

  def widgetProposingUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("proposing-user")
      .exec(getFirstProposalAndVoteAndQualify(questionSlug))
      .exec(Requests.startSequenceByQuestion(enableTracking = false))
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val first = session("firstProposal").as[(String, String)]
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set(
          "proposals",
          Seq(first) ++
            proposals.zip(proposalKeys).take((proposals.size + 1) / 2)
        )
      }
      .exec(voteAndQualifyProposals)
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec(registerAndSubmitProposal)
      .exec(Requests.tracking("click-back-button-after-proposal-submit"))
      .exec(Requests.startSequenceByQuestion())
      .exec { session =>
        val proposals = session("proposalIds").as[Seq[String]]
        val proposalKeys = session("proposalKeys").as[Seq[String]]
        session.set(
          "proposals",
          proposals
            .zip(proposalKeys)
            .slice(proposals.size / 2, proposals.size / 2)
        )
      }
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec(voteAndQualifyProposals)

  def signUpUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("sign-up-user")
      .exec(normalUser(questionSlug))
      .pause(pauseConfiguration.mediumPauses.minimumPause, pauseConfiguration.mediumPauses.maximumPause)
      .exec(register)

  def widgetSignUpUser(questionSlug: String)(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder =
    scenario("sign-up-user")
      .exec(widgetNormalUser(questionSlug))
      .pause(pauseConfiguration.mediumPauses.minimumPause, pauseConfiguration.mediumPauses.maximumPause)
      .exec(register)

  def widgetDisplayOnlyUser(questionSlug: String): ScenarioBuilder =
    scenario("no-interaction")
      .exec(firstProposal(questionSlug))

  def acceptProposals(questionSlug: String, moderatorLogin: String, moderatorPassword: String): ScenarioBuilder = {
    scenario("accept-proposals")
      .exec(Requests.questionDetails(questionSlug))
      .repeat(100) {
        exec(Requests.login(moderatorLogin, moderatorPassword))
          .repeat(1000) {
            exec(Requests.nextProposalToModerate("${questionId}")).exitHereIfFailed
              .exec(Requests.accept("${proposalId}", "${questionId}"))
          }
      }
  }

  def getFirstProposalAndVoteAndQualify(
    questionSlug: String
  )(implicit pauseConfiguration: PauseConfiguration): ScenarioBuilder = {
    scenario("first-proposal-vote-qualify")
      .exec(firstProposal(questionSlug))
      .pause(pauseConfiguration.smallPauses.minimumPause, pauseConfiguration.smallPauses.maximumPause)
      .exec { session =>
        val proposal = session("proposalId").as[String]
        val proposalKey = session("proposalKey").as[String]
        session.set("firstProposal", proposal -> proposalKey)
      }
      .exec(voteAndQualify)
      .pause(pauseConfiguration.mediumPauses.minimumPause, pauseConfiguration.mediumPauses.maximumPause)
      .exec(Requests.tracking("click-sequence-next-card"))
  }
}

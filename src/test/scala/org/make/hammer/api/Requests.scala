package org.make.hammer.api

import io.circe.Encoder
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder
import org.make.hammer.util.{HttpCodes, JsonObjectBody}
import org.make.hammer.util.OAuth._

object Requests {

  case class TrackingRequest(eventName: String, eventType: String, eventParameters: Map[String, String])

  object TrackingRequest {
    implicit val encoder: Encoder[TrackingRequest] =
      io.circe.generic.semiauto.deriveEncoder[TrackingRequest]
  }

  def tracking(eventName: String, parameters: Map[String, String] = Map()): HttpRequestBuilder =
    http(s"tracking $eventName")
      .post("/tracking/front")
      .body(
        JsonObjectBody(TrackingRequest(eventName = eventName, eventType = "trackCustom", eventParameters = parameters))
      )
      .asJson
      .check(status.is(HttpCodes.NoContent))

  def frontConfiguration: HttpRequestBuilder =
    http("front-configuration")
      .get("/configurations/front")
      .check(status.is(HttpCodes.OK))

  def getMe: HttpRequestBuilder =
    http("get-me").get("/user/me")

  def searchProposal(searchParameters: Map[String, String]): HttpRequestBuilder =
    http("search-proposal")
      .get("/proposals")
      .queryParamMap(searchParameters)

  def getOperations: HttpRequestBuilder =
    http("get operations")
      .get("/operations")

  def getQuestionDetails(slug: String): HttpRequestBuilder =
    http("question details")
      .get(s"/questions/$slug/details")

  def getquestionDetails(slug: String): HttpRequestBuilder =
    http("get question details")
      .get(s"/questions/$slug/details")

  def questionDetails(slug: String): HttpRequestBuilder =
    getQuestionDetails(slug)
      .check(status.is(HttpCodes.OK))
      .check(
        jsonPath("$.questionId")
          .saveAs("questionId")
      )

  def getTags: HttpRequestBuilder =
    http("get tags")
      .get("/tags")
      .check(status.is(HttpCodes.OK))

  def startSequenceByQuestion(enableTracking: Boolean = true): HttpRequestBuilder =
    http("start sequence")
      .get("/sequences/standard/${questionId}")
      .header("x-make-location", "sequence ${questionId}")
      .check(status.is(HttpCodes.OK))
      .resources((if (enableTracking) {
                    Seq(Requests.tracking("display-sequence"), Requests.tracking("display-sequence-intro-card"))
                  } else {
                    Seq.empty
                  }): _*)
      .check(jsonPath("$.proposals[*].id").findAll.saveAs("proposalIds"))
      .check(jsonPath("$.proposals[*].proposalKey").findAll.saveAs("proposalKeys"))

  def firstSequenceProposal: HttpRequestBuilder =
    http("get first proposal")
      .get("/sequences/standard/${questionId}/first-proposal")
      .header("x-make-location", "sequence ${questionId}")
      .check(status.is(HttpCodes.OK))
      .resources(Requests.tracking("display-sequence"), Requests.tracking("display-sequence-intro-card"))
      .check(jsonPath("$.proposal.id").find.saveAs("proposalId"))
      .check(jsonPath("$.proposal.proposalKey").find.saveAs("proposalKey"))

  def vote: HttpRequestBuilder =
    http("vote")
      .post("/proposals/${proposalId}/vote")
      .header("x-make-location", "sequence ${questionId}")
      .body(StringBody("""{"voteKey": "${voteKey}", "proposalKey": "${proposalKey}"}"""))
      .asJson
      .resources(Requests.tracking("click-proposal-vote"))
      .check(status.is(HttpCodes.OK))
      .check(
        jsonPath("$.${voteKey}.qualifications[*].qualificationKey").findAll
          .saveAs("qualifications")
      )

  def qualify: HttpRequestBuilder =
    http("qualify proposal")
      .post("/proposals/${proposalId}/qualification")
      .header("x-make-location", "sequence ${questionId}")
      .body(
        StringBody(
          """{"voteKey": "${voteKey}", "qualificationKey": "${qualificationKey}", "proposalKey": "${proposalKey}"}"""
        )
      )
      .asJson
      .check(status.is(HttpCodes.OK))
      .resources(Requests.tracking("click-proposal-qualify"))

  def register: HttpRequestBuilder =
    http("regsiter user")
      .post("/user")
      .body(StringBody("""
            |{
            |  "country": "FR",
            |  "dateOfBirth": "1976-01-01",
            |  "email": "yopmail+${token}@make.org",
            |  "firstName": "${token}",
            |  "language": "fr",
            |  "password": "pAssw0_rd",
            |  "postalCode": "12345",
            |  "profession": "des choses",
            |  "approvePrivacyPolicy": true
            |}
        """.stripMargin))
      .asJson
      .check(status.is(HttpCodes.Created))

  def login: HttpRequestBuilder = login("yopmail+${token}@make.org", "pAssw0_rd")

  def login(login: String, password: String): HttpRequestBuilder =
    http("login")
      .post("/oauth/access_token")
      .formParam("grant_type", "password")
      .formParam("username", login)
      .formParam("password", password)
      .asFormUrlEncoded
      .check(status.is(HttpCodes.OK))
      .check(jsonPath("$.access_token").saveAs("accessToken"))

  def propose: HttpRequestBuilder =
    http("submit proposal")
      .post("/proposals")
      .body(StringBody("""
            |{
            |  "content": "Il faut tester la tenue en charge de l'API",
            |  "country": "FR",
            |  "language": "fr",
            |  "questionId": "${questionId}",
            |  "isAnonymous": false
            |}
        """.stripMargin))
      .asJson
      .check(status.is(HttpCodes.Created))

  def search(parameters: Map[String, String]): HttpRequestBuilder =
    http("search proposals")
      .get("/proposals")
      .queryParamMap(parameters)
      .check(status.is(HttpCodes.OK))

  def nextProposalToModerate(questionId: String): HttpRequestBuilder =
    http("next_proposal_to_moderate")
      .post("/moderation/proposals/next")
      .auth(oauth2("${accessToken}"))
      .header("x-make-app-name", "backoffice")
      .body(StringBody(s"""{
             |  "questionId": "$questionId",
             |  "toEnrich": false,
             |  "minVotesCount": 0,
             |  "minScore": 0
             |}
             |""".stripMargin))
      .asJson
      .check(status.is(HttpCodes.OK))
      .check(jsonPath("$.id").saveAs("proposalId"))

  def accept(proposalId: String, questionId: String): HttpRequestBuilder =
    http("accept_proposal")
      .post(s"/moderation/proposals/$proposalId/accept")
      .auth(oauth2("${accessToken}"))
      .header("x-make-app-name", "backoffice")
      .body(StringBody(s"""{
             |  "sendNotificationEmail": false,
             |  "tags": [],
             |  "questionId": "$questionId",
             |  "predictedTags": [],
             |  "predictedTagsModelName": "none"
             |}
             |""".stripMargin))
      .asJson
      .check(status.is(HttpCodes.OK))

}

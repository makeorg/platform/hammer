package org.make.hammer.util

object HttpCodes {
  val OK = 200
  val Created = 201
  val NoContent = 204
}

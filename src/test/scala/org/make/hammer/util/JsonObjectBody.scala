package org.make.hammer.util

import io.circe.Encoder
import io.circe.syntax._
import io.gatling.core.body.{Body, StringBody}
import io.gatling.core.config.GatlingConfiguration
import io.gatling.core.session

object JsonObjectBody {
  def apply[T](obj: session.Expression[T])(implicit encoder: Encoder[T], configuration: GatlingConfiguration): Body = {

    StringBody(s => obj(s).map(_.asJson.toString()))

  }
}

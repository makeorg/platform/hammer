package org.make.hammer.util

import io.gatling.core.session.Expression
import io.gatling.http.client.realm.{BasicRealm, Realm}
import io.gatling.http.request.builder.HttpRequestBuilder

object OAuth {
  implicit class RicherDsl(val self: HttpRequestBuilder) extends AnyVal {
    def auth(realm: Expression[Realm]): HttpRequestBuilder = {
      import com.softwaremill.quicklens._
      new HttpRequestBuilder(self.commonAttributes.modify(_.realm).setTo(Some(realm)), self.httpAttributes)
    }
  }

  def oauth2(token: Expression[String]): Expression[Realm] = token(_).map(t => Bearer(t))

  final case class Bearer(token: String) extends BasicRealm("none", "none") {
    override def getAuthorizationHeader: String = s"Bearer $token"
  }
}

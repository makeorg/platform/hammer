package org.make.hammer

import java.net.URL

import com.typesafe.config.{ConfigException, ConfigFactory}

import scala.util.Try
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.DurationInt

trait SimulationConfig {

  val statusOk = 200
  val statusCreated = 201
  val statusForbidden = 401

  /**
    * Application config object.
    */
  private[this] val config = ConfigFactory.load()

  def getRequiredString(path: String): String = {
    Try(config.getString(path)).getOrElse {
      handleError(path)
    }
  }

  def getRequiredInt(path: String): Int = {
    Try(config.getInt(path)).getOrElse {
      handleError(path)
    }
  }

  def getRequiredStringList(path: String): java.util.List[String] = {
    Try(config.getStringList(path)).getOrElse {
      handleError(path)
    }
  }

  private[this] def handleError(path: String) = {
    val errMsg = s"Missing required configuration entry: $path"
    throw new ConfigException.Missing(errMsg)
  }

  val baseURL: String = getRequiredString("service.host")
  val users: Int = getRequiredInt("service.users")
  val rampDuration: Int = getRequiredInt("service.ramp-duration")
  val customerLink: String = getRequiredString("service.api-link")
  val hostname: String =
    if (config.hasPath("service.hostname")) {
      getRequiredString("service.hostname")
    } else {
      new URL(baseURL).getHost
    }

  val defaultUserAgent =
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
  val defaultAcceptLanguage = "fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4"

  val adminAuthParams: UserAuthParams = UserAuthParams(
    username = getRequiredString("default-admin.email"),
    password = getRequiredString("default-admin.password")
  )
}

case class UserAuthParams(username: String, password: String)

case class ProposalCreateParams(content: String)

case class Pause(minimumPause: FiniteDuration, maximumPause: FiniteDuration)

case class PauseConfiguration(smallPauses: Pause, mediumPauses: Pause, longPauses: Pause)

object PauseConfiguration {
  val regularUserPausesConfiguration: PauseConfiguration = PauseConfiguration(
    smallPauses = Pause(2.seconds, 5.seconds),
    mediumPauses = Pause(2.seconds, 8.seconds),
    longPauses = Pause(10.seconds, 1.minute)
  )
  val testPausesConfiguration: PauseConfiguration = PauseConfiguration(
    smallPauses = Pause(10.milliseconds, 50.milliseconds),
    mediumPauses = Pause(10.milliseconds, 50.milliseconds),
    longPauses = Pause(10.milliseconds, 50.milliseconds)
  )
}

package org.make.hammer.regression

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import org.make.hammer.SimulationConfig
import org.make.hammer.api.ApiSimulation
import org.make.hammer.PauseConfiguration

class SequenceRegressionSimulation extends Simulation with SimulationConfig {

  val httpConf: HttpProtocolBuilder = http
    .baseUrls(baseURL.split(","): _*)
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader(defaultAcceptLanguage)
    .userAgentHeader(defaultUserAgent)
    .header("x-make-operation", "")
    .header("x-make-source", "core")
    .header("x-make-location", "homepage")
    .header("x-make-question", "")
    .disableCaching

  implicit val pauseConfiguration = PauseConfiguration.testPausesConfiguration

  setUp(
    scenario("Non regression tests")
      .exec(ApiSimulation.proposingUser("load-testing"))
      .inject(atOnceUsers(1))
      .protocols(httpConf)
  ).assertions(forAll.failedRequests.count.is(0L))

}

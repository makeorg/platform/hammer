import sbt.librarymanagement.Resolver._

addSbtPlugin("org.make"       % "sbt-swift-plugin"       % "1.0.9")
addSbtPlugin("io.gatling"     % "gatling-sbt"            % "3.1.0")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.scalameta"  % "sbt-scalafmt"           % "2.4.3")

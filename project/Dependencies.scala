import sbt._

object Dependencies {

  val gatlingVersion = "3.3.1"
  val circeVersion: String = "0.9.3"

  val gatlingHighcharts: ModuleID =
    "io.gatling.highcharts"               % "gatling-charts-highcharts" % gatlingVersion
  val gatling: ModuleID = "io.gatling"    % "gatling-test-framework"    % gatlingVersion
  val circeGeneric: ModuleID = "io.circe" %% "circe-generic"            % circeVersion
}

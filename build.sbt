import java.time.LocalDate

name := "hammer"

version := "0.1"

scalaVersion := "2.12.12"

libraryDependencies ++= Seq(Dependencies.gatling, Dependencies.gatlingHighcharts, Dependencies.circeGeneric)

addCompilerPlugin("org.psywerx.hairyfotr" %% "linter" % "0.1.17")

enablePlugins(SbtSwift)

swiftContainerName      := "reports"
swiftConfigurationPath  := file("/var/run/secrets/hammer.conf")
swiftContainerDirectory := Some("hammer/" + LocalDate.now().toString)
swiftReportsToSendPath  := (Gatling / target).value

enablePlugins(GatlingPlugin)

addCommandAlias("checkStyle", ";scalastyle;test:scalastyle;scalafmtCheckAll;scalafmtSbtCheck")
addCommandAlias("fixStyle", ";scalafmtAll;scalafmtSbt")
